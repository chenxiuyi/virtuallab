Tutorial 3
=================

Heat by Induction to Verify Extremes (HIVE) is an experimental facility at the UK Atomic Energy Authority (UKAEA) to expose plasma-facing components to the high thermal loads that they will experience in a fusion reactor. Samples are thermally loaded by induction heating whilst being actively cooled with pressurised water.

While **Code_Aster** has no in-built ElectroMagnetic coupling, having a python interpreter and being open source makes it easier to couple with external solvers and software compared with proprietary commercial FE codes.

In **VirtualLab**, the heating generated by the induction coil is calculated by using the open source EM solver **ERMES** during the pre-processing stage. The results are piped to **Code_Aster** to be applied as boundary conditions (BC).

The effect of the coolant is modelled as a 1D problem using its temperature, pressure and velocity along with knowing the geometry of the pipe. This version of the code is based on an implementation by Simon McIntosh (UKAEA) of Theron D. Marshall's (CEA) Film-2000 software to model the Nukiyama curve :cite:`film2000` for water-cooled fusion divertor channels, which itself was further developed by David Hancock (also UKAEA). The output from this model is also piped to **Code_Aster** to apply as a BC.

.. admonition:: Action
   :class: Action

   For this tutorial the the *RunFile* should have the values::

        Simulation='HIVE'
        Project='Tutorials'
        Parameters_Master='TrainingParameters'
        Parameters_Var=None

        VirtualLab=VLSetup(
                   Simulation,
                   Project)

        VirtualLab.Settings(
                   Mode='Interactive',
                   Launcher='Process',
                   NbJobs=1)

        VirtualLab.Parameters(
                   Parameters_Master,
                   Parameters_Var,
                   RunMesh=True,
                   RunSim=True,
                   RunDA=True)

        VirtualLab.Mesh(
                   ShowMesh=False,
                   MeshCheck=None)

        VirtualLab.Sim(
                   RunPreAster=True,
                   RunAster=True,
                   RunPostAster=True,
                   ShowRes=True)

        VirtualLab.DA()

        VirtualLab.Cleanup()

In :file:`Input/HIVE/Tutorials/TrainingParameteres.py` you will notice at the top there is a flag, *EMLoad*, which indicates how the thermal load generated by the coil will be modelled. The options are either via a uniform heat flux or using the **ERMES** solver.

Sample
******

The sample selected to use in this tutorial is an additive manufactured sample which was part of the EU FP7 project "Additive Manufacturing Aiming Towards Zero Waste & Efficient Production of High-Tech Metal Products" (AMAZE, grant agreement No. 313781). The sample is a copper block on a copper pipe with a tungsten tile on the top.

The file used to generate the mesh is :file:`Scripts/HIVE/Mesh/AMAZE.py`. The geometrical parameters, referenced in :numref:`Fig. %s <AMAZE>`, are::

    Mesh.BlockWidth = 0.03
    Mesh.BlockLength = 0.05
    Mesh.BlockHeight = 0.02
    Mesh.PipeCentre = [0,0]
    Mesh.PipeDiam = 0.01
    Mesh.PipeThick = 0.001
    Mesh.PipeLength = Mesh.BlockLength
    Mesh.TileCentre = [0,0]
    Mesh.TileWidth = Mesh.BlockWidth
    Mesh.TileLength = 0.03
    Mesh.TileHeight = 0.005

.. _AMAZE:

.. figure :: https://gitlab.com/ibsim/media/-/raw/master/images/VirtualLab/AMAZE.png?inline=false

    Drawing of the AMAZE sample with the attirubtes of ``Mesh`` used to specify the dimensions.

The centre of the pipe is offset from the centre of the co-planar block face by *PipeCentre*. Simialrly the centre of the tile is offset from the centre of the block face by *TileCentre*.

The attributes *Length1D*-*3D* again specify the mesh refinement::

    # Mesh parameters
    Mesh.Length1D = 0.005
    Mesh.Length2D = 0.005
    Mesh.Length3D = 0.005
    Mesh.PipeSegmentN = 20
    Mesh.SubTile = 0.002

The attribute *PipeSegmentN* specifies the number of segments the pipe circumference will be split into. Due to the induction heating primarily being subjected to the tile on the sample, a finer mesh is required in this location. The attribute *SubTile* specifies the mesh size (1D, 2D and 3D) on the tile.

Simulation
***********

You will notice in *Parameters_Master* that ``Sim`` has the attribute *PreAsterFile* set to *PreHIVE*. The file :file:`Scripts/HIVE/Sim/PreHIVE.py` calculates the HTC between the pipe and the coolant for a range of temperatures. ::

    Sim.CreateHTC = True
    Sim.Pipe = {'Type':'smooth tube', 'Diameter':0.01, 'Length':0.05}
    Sim.Coolant = {'Temperature':20, 'Pressure':2, 'Velocity':10}

The dictionary *Pipe* specifies information about the geometry of the pipe, while *Coolant* provides properties about the fluid in the pipe. *CreateHTC* is a boolean flag to indicate if this step is run or if previously calculated values are used.

If **ERMES** is to be used for the thermal loading, then this is also launched in this script using the attributes::

    Sim.RunERMES = True
    Sim.CoilType = 'Test'
    Sim.CoilDisplacement = [0,0,0.0015]
    Sim.Rotation = 0

    Sim.NbProc = 1
    Sim.Current = 1000
    Sim.Frequency = 1e4

    Sim.Threshold = 1
    Sim.NbClusters = 100

**ERMES** requires a mesh of the induction coil and surrounding vacuum which must conform with the mesh of the component.

The attribute *CoilType* specifies the coil design to be used. Currently available options are:

* 'Test'
* 'HIVE'

*CoilDisplacement* dictates the x,y and z components of the displacement of the coil with respect to the sample. The z-component indicates the gap between the upper surface of the sample and the coil and must be positive. The x and y components indicate the coil's offset about the centre of the sample.

The sample is fitted in HIVE using the pipe, meaning that there is an additional rotational degree of freedom available.

*Current* and *Frequency* are used by **ERMES** to produce a range of EM results, such as the Electric field (E), the Current density (J) and Joule heating. These results are stored in the sub-directory *PreAster* within the simulation directory. *NbProc* dictates how many cpus  **ERMES** is entitled to use for each simulation.

The Joule heating profile is used by **Code_Aster** to apply the thermal loads. A mesh group is required for each individual volumetric element within the mesh to apply the heat source, however doing so substantially increases the computation time. Two approaches are available to reduce the computation time; thresholding and clustering.

Thresholding takes the approach that the most influential thermal loads occur in the region of the sample nearest the coil, meaning that the majority of the mesh groups have little impact on the results.

:numref:`Fig. %s <EM_Thresholding>` shows that, for a particular setup, 99% of the power generated by the coil is applied through less than 18% of the elements. As a result only 3660 mesh groups would be required instead of 20494.

.. _EM_Thresholding:

.. figure :: https://gitlab.com/ibsim/media/-/raw/master/images/VirtualLab/EM_Thresholding.png?inline=false

    Semi-log plot showing the fraction of elements needed to reach 50%, 90%, 99%, 99.9%, 99.99% and 100% of the coil power. The power delivered by the coil has been normalised.

.. note::

    The coil power percentages in :numref:`Fig. %s <EM_Thresholding>` are an example only. These values will vary drastically depending on such things as the mesh refinement, frequency in the coil etc.

The attribute *Threshold* specifies the fraction of the total coil power that has been selected to use as a 'cut-off'.

Although thresholding reduces the number of mesh groups, for a finer mesh the number of groups will still be large, resulting in increased computation time. Clustering on the other hand groups the Joule heating distribution in to N-number of groups or 'bins'.

The 1D k-means algorithm (also known as the Jenks optimisation method) find the N optimal value to group the distribution in to. The Goodness of Fit Value (GFV) describes how well the clustering represents the data, ranging from 0 (worst) to 1 (best).

The attribute *NbClusters* specifies the number of groups to cluster the data in to. This method overcomes the drawbacks of thresholding, as finer meshes will still be accurately represented by the N clusters. In this analysis no thresholding will be used and 100 clusters are used.

The *RunERMES* flags works similarly to *CreateHTC*.

As the loads are not time-dependent this can be treated as a stationary thermal problem, with the command file :file:`AMAZE_SS.comm` used (SS=Steady State). A transient version of this simulation is also available, :file:`AMAZE.comm`.

Task 1: Uniform Heat Flux
**************************

You will notice in *Parameters_Master* that if *EMLoad* is set to 'Uniform' the only additional argument required for the analysis is the magnitude of the heat flux, *Sim.Flux*.

.. admonition:: Action
   :class: Action

   Ensure *EMLoad* is set to 'Uniform' at the top of :file:`TrainingParameters.py` and launch **VirtualLab**::

        VirtualLab -f RunFiles/RunTutorials.py

A sub-directory named 'Examples' will have been created in the project directory, inside which the results of this simulation can be found.

The data used for the HTC between the coolant and the pipe is saved to :file:`PreAster/HTC.dat` in the simulation directory along with a plot of the data :file:`PipeHTC.png`

By looking at the results in **ParaVis** it should be clear that the heat is applied uniformly to the top surface. You should also be able to see the effect that the HTC BC is having on the pipe's inner surface.

Task 2: Running an ERMES simulation
************************************

While the uniform simulation is useful it is an unrealistic model of the heat source produced by the induction coil. A more accurate heating profile can be achieved using **ERMES** .

.. admonition:: Action
   :class: Action

   In :file:`TrainingParameters.py` change *EMLoad* to 'ERMES' and change the name for the simulation::

      EMLoad = 'ERMES'

      Sim.Name = 'Examples/ERMES'

   Since the same mesh can be used, *RunMesh* can be set to to :code:`False` in `VirtualLab.Parameters <../runsim/runfile.html#virtuallab-parameters>`_ in the *RunFile*. Also change the *RunAster* ``kwarg`` to :code:`False` in `VirtualLab.Sim <../runsim/runfile.html#virtuallab-sim>`_ as we are only interested in the **ERMES** simulation::

      VirtualLab.Parameters(
                 Parameters_Master,
                 Parameters_Var,
                 RunMesh=False,
                 RunSim=True,
                 RunDA=True)

      VirtualLab.Sim(
                 RunPreAster=True,
                 RunAster=False,
                 RunPostAster=True,
                 ShowRes=True)

   Launch **VirtualLab**.

Information generated by the **ERMES** solver is printed to the terminal followed by the power which is imparted in to the sample by the coil, which should be 127.23 W.

The results generated by **ERMES** are converted to a format compatible with **ParaVis** and saved to :file:`PreAster/ERMES.rmed`. These are the results which are displayed in the GUI, assuming the ``kwarg`` *ShowRes* is still set to :code:`True`.

The results from **ERMES** show's the whole domain, which includes the volume surrounding the sample and coil, which will obscure the view of them. In order to only visualise the sample and coil, these groups must be extracted. This is accomplished by selecting ``Filters / Alphabetical / Extract Group`` from the menu, then using the checkboxes in the properties window (usually on the bottom left side) to select ``Coil`` and ``Sample`` before clicking ``Apply``.

It should then be possible to visualise any of the following results:

 * Joule_heating
 * Electric field (E) - real, imaginary and modulus
 * Magnetic field (H) - real, imaginary and modulus
 * Current Density (J) - real, imaginary and modulus

Joule_heating is the field which is used in **Code_Aster**.

Task 3: Applying ERMES BC in Code_Aster
****************************************

Next a thermal simulation is performed by **Code_Aster** using the results from **ERMES**. As it's the steady state we are interested in there is no need to run a transient simulation, reducing the computation time substantially.

Since the HTC and **ERMES** data have already been generated there is no need to run these again.

.. admonition:: Action
   :class: Action

   In :file:`TrainingParameters.py` set *CreateHTC* and *RunERMES* to :code:`False`. The values for **Threshold** and **NbClusters** are already set::

      Sim.CreateHTC=False
      Sim.RunERMES=False

   You will also need to change the ``kwarg`` *RunAster* back to :code:`True` in the *RunFile* to run the simulation::

      VirtualLab.Sim(
                 RunPreAster=True,
                 RunAster=True,
                 RunPostAster=True,
                 ShowRes=True)

   Launch **VirtualLab**.

Both the **ERMES** and **Code_Aster** results are displayed in **ParaVis** with the suffix 'ERMES' and 'Thermal' respectively.

By investigating the visualisation of the **Code_Aster** results you will observe that the heating profile in the sample by using this coil is more representative of 'real world' conditions. You should also notice that the temperature profile on the sample is very similar to the *Joule_heating* profile generated by **ERMES**.

Task 4: Scaling ERMES
**********************

Because **ERMES** is a linear solver, the results generated are proportional to the current in the coil. This means that if we wanted to re-run analyses with a different current it is not necessary to re-run **ERMES**.

.. warning::
    The same is not true for *Frequency* as this is used in the non-linear cos and sin functions. If the frequency is changed **ERMES** will need to be re-run.

The **ERMES** results E,H and J all scale linearly with *Current*. Since *Joule_heating* is the product of E and J it is proportional to the square of the *Current*. The power is calculated using *Joule_heating* and so this is also proprtional to the square.

In this case, we decide that we want to run another transient simulation where the power input to the component using **ERMES** is equal to that of the uniform simulation. In the uniform simulation, a flux of 1e6 W/m^2 was applied over a surface of 9e-4 m^2 (0.03m x 0.03m), resulting in 900 W. The power generated by the **ERMES** simulation in Task 2 was 127.2284 W. Therefore the current must be scaled by :math:`\sqrt{\dfrac{900}{127.2284}} = 2.65967...`

We do not want to overwrite the results of the previous simulation. This can be achieved by copying the existing output from Task 3 into a new directory.

.. admonition:: Action
   :class: Action

   Create a copy of the directory 'ERMES' in :file:`Output/HIVE/Tutorials/Examples` and name it 'ERMES_2'.

   In :file:`TrainingParameters.py` you will need to change *Sim.Name* to 'Examples/ERMES_2' and multiply the value for the attribute *Current* by 2.6597::

      Sim.Name = 'Examples/ERMES_2'
      Sim.Current = 1000*2.6597

   Launch **VirtualLab**.

This will overwrite the **Code_Aster** results copied across to 'ERMES_2' with new results based on a linear scaling of the original **ERMES** calculations without re-running it.

You should notice that with this scaling the power input is 900 W (some slight error may be due to rounding), which is printed to the terminal.

Open the **Code_Aster** results from 'Uniform' in **ParaVis** alongside those from 'ERMES_2' in ``File/Open ParaView File``. The maximum temperature for the sample in 'ERMES_2' will be higher than that of 'Uniform' due to hotspots increased created by the coil design.
