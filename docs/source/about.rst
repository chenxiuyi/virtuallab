About
=====

**VirtualLab** is the ouput of a collaboration between the `Image-Based Simulation <https://ibsim.co.uk>`_ (IBSim) Group at `Swansea University <https://www.swansea.ac.uk>`_ and `UK Atomic Energy Authority <https://ccfe.ukaea.uk/>`_. The majority of the initial work was carried out by Rhydian Lewis during his PhD on *"Using machine learning to maximise impact of fusion energy experimental facility virtual twin"*.

Support
*******

If you are having issues using **VirtualLab**, please let us know.

You may contact us by raising an issue on the project's `gitlab repository <https://gitlab.com/ibsim/virtuallab/-/issues>`_.

Attribution
***********

At the IBSim group, we are firm believers in open-access data and open-source code. Our main goal is that the output of our research will lead to wider use of virtual testing techniques, particularly for industrial applications. Therefore, access to the resources we develop is offered freely. This is with the aim that they will be used as part of your application.

The **VirtualLab** code is licensed under the Apache License, Version 2.0. That is, the user may distribute, remix, tweak, and build upon the licensed work, including for commercial purposes, as long as the original author is credited. We would also ask that you kindly inform us (resources@ibsim.co.uk) if the use of our resources has led to further output (e.g. industrial application, new software, journal publications). This enables us to evidence the value of our research when we apply for funding which will lead to further resource development for your benefit.

To cite this code in your publication please use the format below:

  R. Lewis, Ll.M. Evans (2020) VirtualLab source code (Version !!!) [Source code]. https://gitlab.com/ibsim/virtuallab/-/commit/3c1d7727987def758df32a34933c964f54579325

**NOTE:** You will need to update the version number and url to the commit version you used.

Contribute
**********

There are several ways to contribute to **VirtualLab**, all of which require a GitLab Account:

1. Use it and report issues, bugs and potential features that would benefit **VirtualLab**!

  https://gitlab.com/ibsim/virtuallab/-/issues

2. If you are a developer, the source code of **VirtualLab** is publicly available. Feel free to have a look at it, find and fix bugs, add new features or rework some areas of the tool and submit them as Pull Requests (PR).

  https://gitlab.com/ibsim/virtuallab/-/tree/master

3. Similarly, this web-based documentation is also publicly available from the **VirtualLab** repository. New content for the documentation, correction typos or new tutorials in the form of PR are very welcome.

License
*******

Copyright 2020 IBSim Group (c/o Llion Marc Evans).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.

You may obtain a copy of the License at:

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

