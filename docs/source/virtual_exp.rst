Virtual Experiments
===================

Tensile Testing
***************

A tensile test is a standard mechanical experiment where a 'dog-bone' shaped sample is loaded. The load can be applied as a constant force whilst measuring the displacement or as a constant displacement whilst measuring the required load. This provides information about mechanical properties such as `Young's elastic modulus <https://en.wikipedia.org/wiki/Young%27s_modulus>`_.

Laser Flash Analysis
********************

Laser flash analysis (LFA) is an experiment where a disc shaped sample has a short laser pulse incident on one surface, whilst the temperature change is tracked with respect to time on the opposing surface. This is used to measure thermal diffusivity, which is used to calculate thermal conductivity.

HIVE
****

Heat by Induction to Verify Extremes (HIVE) is an experimental facility at the UK Atomic Energy Authority's (UKAEA) Culham site. It is used to expose plasma-facing components to the high thermal loads they will be subjected to in a fusion energy device. In this experiment, samples are thermally loaded on one surface by induction heating whilst being actively cooled with pressurised water.
