#usr/bin/env python

from .Salome.Salome import Run as SalomeRun
from .CodeAster.Aster import Run as AsterRun
from .ERMES.ERMES import Run as ERMESRun
